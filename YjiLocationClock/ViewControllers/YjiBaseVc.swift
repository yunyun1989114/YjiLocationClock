//
//  YjiBaseVc.swift
//  YjiPhotoViewer
//
//  Created by jiyun on 2016/03/07.
//  Copyright © 2016年 Ericji. All rights reserved.
//

import UIKit

class YjiBaseVc: UIViewController {
    
    var mModalView: UIView?
    var mLoadingView: UIView?
    var mIndicator: UIActivityIndicatorView?
    
    var mHideNaviBar = false {
        willSet {
            self.hideNaviBar(newValue)
        }
    }
    
    var mTitle: String? {
        willSet {
            self.setCustomTitle(newValue!)
        }
    }
    
    var mBackBtn = UIButton(frame: CGRectMake(0, 0, 15, 25))

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // navi bar defalut
        self.hideNaviBar(mHideNaviBar)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 1.0/255.0, green: 218.0/255.0, blue: 199.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.translucent = false
        // defalut have back button
        mBackBtn.setImage(UIImage(named: "backBtn"), forState: .Normal)
        mBackBtn.addTarget(self, action:#selector(self.popViewController(_:)), forControlEvents: .TouchUpInside)
        let backItem = UIBarButtonItem(customView: mBackBtn)
        self.navigationItem.leftBarButtonItem = backItem
        
        // setぐるぐる画面
        mModalView = UIView.init(frame: self.view.bounds)
        mModalView?.backgroundColor = UIColor.clearColor() // to protect user from handling
        mLoadingView = UIView.init(frame: CGRectMake(0, 0, 100, 100))
        mLoadingView?.center = CGPointMake(YjiComFuncs.getScreenWidth() / 2, YjiComFuncs.getScreenHeight() / 2)
        mLoadingView!.backgroundColor = UIColor.grayColor()
        mLoadingView!.alpha = 1
        mLoadingView?.cornerRadius = 10
        
        mIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .WhiteLarge)
        mIndicator!.center = CGPointMake(mLoadingView!.bounds.size.width / 2, mLoadingView!.bounds.size.height / 2)
        mLoadingView!.addSubview(mIndicator!)
        mModalView!.addSubview(mLoadingView!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func hideNaviBar(isNeed: Bool) {
        self.navigationController?.navigationBar.hidden = isNeed
    }
    
    func setCustomTitle(titleStr: String) {
        let titLbl = UILabel(frame: CGRectMake(0, 0, 100, 44))
        titLbl.text = titleStr
        titLbl.textAlignment = .Center
        titLbl.textColor = UIColor.whiteColor()
        titLbl.font = UIFont.boldSystemFontOfSize(18)
        self.navigationItem.titleView = titLbl
    }
    
    func popViewController(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func hideBackButton() {
        mBackBtn.hidden = true
    }
    
    // MARK: - ぐるぐる画面表示
    func showProgressView() {
        self.view.addSubview(mModalView!)
        mIndicator?.startAnimating()
    }
    
    func stopProgressView() {
        mModalView?.removeFromSuperview()
        mIndicator?.stopAnimating()
    }

    // alert
    func showAlertWith(title: String, msg: String) {
        if self.presentedViewController == nil {
            let av = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
            av.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                print("User select the OK button")
                YjiLocationManager.sharedInstance.stopUpdateLocation()
            }))
            self.presentViewController(av, animated: true, completion: nil)
        }
    }
    

}
