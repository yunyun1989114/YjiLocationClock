//
//  QVSGMapViewController.swift
//  Monaca
//
//  Created by kiu-cts on 2016/02/12.
//  Copyright © 2016年 CASIO. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import GoogleMaps

class YjiGMapSearchVc: YjiBaseVc, CLLocationManagerDelegate, GMSMapViewDelegate, UISearchBarDelegate {
    var mSearchBar = UISearchBar()
    let mSlider = UISlider()
    var mGmaps = GMSMapView()
    var mYjiLocationManager: YjiLocationManager?
    var mCurrentLocation: CLLocation?
    var mDidFindMyLocation = false
    var mLocationMarker = GMSMarker()
    var mSelectedPosition = CLLocationCoordinate2D()
    var mSelectedLocation: CLLocation?
    var mCircle =  GMSCircle()
    
    // yjitest
    let curLocation1 = UILabel()
    let distanceLbl = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mTitle = "地図"
        
        // right UIBarButtonItem
        let createBtn = UIButton(frame: CGRectMake(0, 0, 40, 20))
        createBtn.setTitle("監視", forState: .Normal)
        createBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        createBtn.addTarget(self, action:#selector(self.startMonitor), forControlEvents: .TouchUpInside)
        let cBarButtonItem = UIBarButtonItem(customView: createBtn)
        self.navigationItem.setRightBarButtonItem(cBarButtonItem, animated: true)
        
        let cancelBtn = UIButton(frame: CGRectMake(0, 0, 40, 20))
        cancelBtn.setTitle("停止", forState: .Normal)
        cancelBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        cancelBtn.addTarget(self, action:#selector(self.cancelMonitor), forControlEvents: .TouchUpInside)
        let cancelBarButtonItem = UIBarButtonItem(customView: cancelBtn)
        self.navigationItem.setLeftBarButtonItem(cancelBarButtonItem, animated: true)
        
        mYjiLocationManager = YjiLocationManager.sharedInstance
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.getAuthorizaion(_:)), name: notificationGpsAuthorizaionGet, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.updateLocation(_:)), name: notificationUpdateLocation, object: nil)
        
        // Do any additional setup after loading the view.
        self.view.addSubview(mGmaps)
        mGmaps.snp_makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(48.857165, longitude: 2.354613, zoom: 8.0)
        mGmaps.camera = camera
        // MapViewをviewに追加する.
        mGmaps.myLocationEnabled = true
        mGmaps.delegate = self
        mGmaps.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
        
        
        // search Bar
        mSearchBar.delegate = self
        self.view.addSubview(mSearchBar)
        mSearchBar.snp_makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.top.equalTo(10)
            make.height.equalTo(40)
        }
        
        mSlider.minimumValue = 500.0
        mSlider.maximumValue = 3000.0
        mSlider.addTarget(self, action: #selector(sliderDidChange(_:)), forControlEvents: .ValueChanged)
        self.view.addSubview(mSlider)
        mSlider.snp_makeConstraints { (make) in
            make.bottom.equalTo(-80)
            make.left.equalTo(20)
            make.right.equalTo(-20)
        }
        mSlider.hidden = true
        
        curLocation1.backgroundColor = UIColor.yellowColor()
        curLocation1.font = UIFont.systemFontOfSize(12)
        curLocation1.textColor = UIColor.blackColor()
        self.view.addSubview(curLocation1)
        curLocation1.snp_makeConstraints { (make) in
            make.right.equalTo(0)
            make.top.equalTo(50)
        }
        
        distanceLbl.backgroundColor = UIColor.yellowColor()
        distanceLbl.font = UIFont.systemFontOfSize(12)
        distanceLbl.textColor = UIColor.greenColor()
        self.view.addSubview(distanceLbl)
        distanceLbl.snp_makeConstraints { (make) in
            make.right.equalTo(0)
            make.top.equalTo(curLocation1.snp_bottom).offset(10)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func sliderDidChange(slider: UISlider) {
        let value = slider.value
        mCircle.radius = CLLocationDistance(value)
    }
    
    // MARK: - notification
    func getAuthorizaion(notice: NSNotification)  {
        mGmaps.myLocationEnabled = true
    }
    
    func updateLocation(notice: NSNotification) {
        let userInfo = notice.userInfo
        let location = userInfo!["newLocation"] as! CLLocation
        let eventDate = location.timestamp
        let time = eventDate.timeIntervalSinceNow
        curLocation1.text = "\(time)"
        
        // calculate the location
        var distance: CLLocationDistance?
        if mSelectedLocation != nil {
            distance = YjiComFuncs.getMeterFrom(location, newLocation: mSelectedLocation!)
            distanceLbl.text = "\(distance)"
            if  distance < mCircle.radius {
                if mYjiLocationManager?.mIsBackgroundState == true {
                    // background handle
                    YjiComFuncs.sendNativeNotification()
                } else {
                    self.showAlertWith("Success", msg: "You have access into the area!")
                }
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        mYjiLocationManager?.stopUpdateLocation()
        mGmaps.removeObserver(self, forKeyPath: "myLocation", context: nil)
        
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if !mDidFindMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
            mGmaps.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: 5.0)
            mGmaps.settings.myLocationButton = true
            mDidFindMyLocation = true
        }
    }
    
    func mapView(mapView: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        if mSearchBar.isFirstResponder() {
            mSearchBar.resignFirstResponder()
        } else {
            self.updateMap(coordinate)
        }
    }
    
    func updateMap(coordinate: CLLocationCoordinate2D) {
        
        
        // to get location Info
        let geocoder: CLGeocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) { (placemarks, error) -> Void in
            if error != nil {
                print(error)
            }
            //            print(placemarks)
            let placemark = placemarks![0]
            
            var title = ""
            if let countryCode = placemark.ISOcountryCode {
                title = title + countryCode
            }
            if let country = placemark.country {
                title = title + country
            }
            
            var snippet = ""
            if let postalCode = placemark.postalCode {
                snippet = snippet + postalCode
            }
            if let administrativeArea = placemark.administrativeArea {
                snippet = snippet + administrativeArea
            }
            if let locality = placemark.locality {
                snippet = snippet + locality
            }
            if let subLocality = placemark.subLocality {
                snippet = snippet + subLocality
            }
            if let subThoroughfare = placemark.subThoroughfare {
                snippet = snippet + subThoroughfare
            }
            
            //            self.locationMarker = GMSMarker(position: coordinate) // multiple mark
            self.mLocationMarker.position = coordinate
            self.mLocationMarker.title = title
            self.mLocationMarker.snippet = snippet
            self.mLocationMarker.map = self.mGmaps
            
            self.reloadViewWithPosition(coordinate)
        }
    }
    
    func reloadViewWithPosition(position: CLLocationCoordinate2D) {
        mGmaps.camera = GMSCameraPosition.cameraWithTarget(position, zoom: 15)
        mSelectedLocation = CLLocation(latitude: position.latitude, longitude: position.longitude)
        
        mCircle.position = position
        mCircle.radius = 500 // default
        mCircle.fillColor = UIColor(red: 0.25, green: 0, blue: 0, alpha: 0.05)
        mCircle.strokeColor = UIColor.redColor()
        mCircle.strokeWidth = 2
        mCircle.map = self.mGmaps
        
        mSlider.hidden = false
//        print(YjiComFuncs.getMeterFrom(mCurrentLocation!, newLocation: CLLocation(latitude: position.latitude, longitude: position.longitude)))
        self.stopProgressView()
    }
    
    func startMonitor() {
        curLocation1.hidden = false
        distanceLbl.hidden = false
        mYjiLocationManager?.startUpdateLocation()
    }
    
    func cancelMonitor() {
        curLocation1.hidden = true
        distanceLbl.hidden = true
        mYjiLocationManager?.stopUpdateLocation()
    }
    
    // MARK:- search delegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.showProgressView()
        searchBar.resignFirstResponder()
        let mapTask = YjiMapTask()
        mapTask.geocodeAddress(searchBar.text) { (status, success) in
            if status == "OK" && success == true {
                let locationInfo = CLLocationCoordinate2DMake(mapTask.mFetchedAddressLatitude, mapTask.mFetchedAddressLongitude)
                self.updateMap(locationInfo)
            } else {
                self.stopProgressView()
                let av = UIAlertController(title: "Failure", message: "Sorry,I can't find the way!", preferredStyle: .Alert)
                av.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                    self.mYjiLocationManager?.stopUpdateLocation()
                }))
                self.presentViewController(av, animated: true, completion: nil)
            }
        }
    }
        
}
