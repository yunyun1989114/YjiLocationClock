//
//  ViewController.swift
//  YjiLocationClock
//
//  Created by 季云 on 16/3/27.
//  Copyright © 2016年 yji. All rights reserved.
//

import UIKit
import SnapKit

class YjiStartVc: YjiBaseVc {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        self.mTitle = "ようこそ"
        self.hideBackButton()
        
        // background image
        let backgroundImageView = UIImageView()
        backgroundImageView.image = UIImage(named: "startView")
        self.view.addSubview(backgroundImageView)
        backgroundImageView.snp_makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        let startBtn = UIButton()
        startBtn.addTarget(self, action: #selector(self.startAction(_:)), forControlEvents: .TouchUpInside)
        startBtn.cornerRadius = 10
        startBtn.backgroundColor = UIColor(red: 1.0/255.0, green: 218.0/255.0, blue: 199.0/255.0, alpha: 1.0)
        startBtn.setTitle("始める", forState: .Normal)
        startBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.view.addSubview(startBtn)
        startBtn.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(-50)
            make.left.equalTo(40)
            make.right.equalTo(-40)
            make.height.equalTo(50)
        }
    }
    
    func startAction(sender: AnyObject) {
        let vc = YjiGMapSearchVc()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }


}

