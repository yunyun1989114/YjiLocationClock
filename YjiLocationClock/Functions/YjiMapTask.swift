//
//  YjiMapTask.swift
//  YjiMap
//
//  Created by kiu-cts on 2016/02/09.
//  Copyright © 2016年 kiu-cts. All rights reserved.
//

import UIKit

class YjiMapTask: NSObject {
    
    let mBaseURLGeocode: NSString = "https://maps.googleapis.com/maps/api/geocode/json?"
    var mLookupAddressResults: NSDictionary!
    var mFetchedFormattedAddress: NSString!
    var mFetchedAddressLongitude: Double!
    var mFetchedAddressLatitude: Double!
    
    override init() {
        super.init()
    }
    
    func geocodeAddress(address: String!, withCompletionHandler completionHandler: ((status: String, success: Bool) -> Void)) {
        
        if let lookupAddress = address {
            let geocodeURLString = mBaseURLGeocode.stringByAppendingString("address=").stringByAppendingString(lookupAddress).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet()) // swift encoding
            let geocodeURL = NSURL.init(string: geocodeURLString!)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let geocodingResultsData = NSData(contentsOfURL: geocodeURL!)
                var dictionary:NSDictionary?
                do {
                   dictionary =  try NSJSONSerialization.JSONObjectWithData(geocodingResultsData!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
                } catch {
                    print(error)
                    completionHandler(status: "", success: false)
                }
                
                let status = dictionary!["status"] as! String
                if status == "OK" {
                    let allResults = dictionary!["results"] as! NSArray
                    self.mLookupAddressResults = allResults[0] as! NSDictionary
                    // Keep the most important values.
                    self.mFetchedFormattedAddress = self.mLookupAddressResults["formatted_address"] as! NSString
                    let geometry = self.mLookupAddressResults["geometry"] as! NSDictionary
                    self.mFetchedAddressLongitude = ((geometry["location"] as! NSDictionary)["lng"] as! NSNumber).doubleValue
                    self.mFetchedAddressLatitude = ((geometry["location"] as! NSDictionary)["lat"] as! NSNumber).doubleValue
                    completionHandler(status: status, success: true)
                }
                else {
                    completionHandler(status: status, success: false)
                }
            })
        }
        
    }
    

}
