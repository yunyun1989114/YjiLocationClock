//
//  YjiComFuncs.swift
//  YjiLocationClock
//
//  Created by 季云 on 16/3/30.
//  Copyright © 2016年 yji. All rights reserved.
//

import UIKit
import GoogleMaps

class YjiComFuncs: NSObject {
    
    class func getMeterFrom(oldLocation: CLLocation, newLocation:CLLocation) -> CLLocationDistance {        
        let meters: CLLocationDistance = newLocation.distanceFromLocation(oldLocation)
        return meters
    }
    
    class func getScreenSize() -> CGSize {
        let screenSize = UIScreen.mainScreen().bounds.size
        return screenSize
    }
    
    class func getScreenWidth() -> CGFloat {
        let width = UIScreen.mainScreen().bounds.width
        return width
    }
    
    class func getScreenHeight() -> CGFloat {
        let height = UIScreen.mainScreen().bounds.height
        return height
    }
    
    // MARK: - native notification
    class func sendNativeNotification() {
        dispatch_async(dispatch_get_main_queue()) {
            let noti = UILocalNotification()
            noti.fireDate = NSDate()
            noti.soundName = UILocalNotificationDefaultSoundName
            noti.alertBody = "すぐに到着しますよ"
            noti.applicationIconBadgeNumber = 1
            UIApplication.sharedApplication().scheduleLocalNotification(noti)
        }
    }
    
}
